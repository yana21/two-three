import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class testTwothree {

        @Test(expected = IllegalArgumentException.class)//ожидаемый результат теста
        public void testDLowerThanZeroOrAIsZero(){
            Twothree th = new Twothree(0,1,1);
            assertNull(th.getRoots());
        }

        @Test
        public void testDEqualsZero(){
            Twothree th = new Twothree(1,2,1);
            assertEquals(th.getRoots()[0],-1, 10E-9);
            assertEquals(th.getRoots()[1],-1,10E-9);
        }

        @Test
        public void testDBiggerThanZero(){
            Twothree th = new Twothree(1,2,-3);
            assertEquals(th.getRoots()[0],1, 10E-9);
            assertEquals(th.getRoots()[1],-3,10E-9);
        }


}
