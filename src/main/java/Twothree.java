public class Twothree {
    private double a;
    private double b;
    private double c;

    public Twothree(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double[] getRoots() {
        double d = b*b - 4*a * c;
        double x1, x2;
        if (d < 0 || a <= 10E-9) {
            throw new IllegalArgumentException();
        } else if (d <= 10E-9) {
            x1 = (-b) / (2. * a);
            x2 = x1;
        } else {
            x1 = (-b + Math.sqrt(d)) / (2 * a);
            x2 = (-b - Math.sqrt(d)) / (2 * a);
        }

        return new double[]{x1, x2};
    }
}
